#!/usr/bin/python
# -*- coding: utf-8 -*-

from sys import exit, argv
from PyQt4.QtGui import QApplication, QWidget, QCheckBox, QPushButton, QLabel, QGridLayout, QIcon, QSizePolicy, QMovie
from PyQt4.QtCore import Qt, QSize, pyqtSlot, pyqtSignal, QPoint, QRect

class Example(QWidget):

    def __init__(self):
        super(Example, self).__init__()
        self.toggled = False
        self.initUI()

    def initUI(self):

        self.layout = QGridLayout()
        self.label = QLabel()
        self.label.setMinimumWidth(100)
        self.label.setMinimumHeight(100)
        self.label.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        self.label_animation = QMovie('circles.gif')
        self.label_animation.setScaledSize(QSize(100, 100))
        self.label.setMovie(self.label_animation)
        self.label_animation.start()
        self.label_animation.setPaused(True)
#        self.label.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.layout.addWidget(self.label, 0, 0)

        self.button = QPushButton()
        self.button.setObjectName('button')
#        self.button.setIcon(QIcon('icon.png'))
        self.button.setStyleSheet('QPushButton#button{border-top-right-radius: 10px; border-bottom-right-radius: 10px; border-top-left-radius: 10px; border-bottom-left-radius: 10px; border-image: url("icon.png"); background: transparent;}')
        self.button.setMinimumWidth(100)
        self.button.setMinimumHeight(100)
        self.button.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        self.button.clicked.connect(self.toggle)
        self.layout.addWidget(self.button, 0, 1)

        self.setGeometry(200, 200, 400, 400)
        self.setWindowTitle('Icons, Images && Video with PyQt')
        self.setLayout(self.layout)
        self.resize(self.layout.sizeHint())

    def resizeEvent(self, event):
      self.label_animation.setScaledSize(QSize(self.label.width(), self.label.height()))

    @pyqtSlot()
    def toggle(self):
      style = 'QPushButton#button{border-top-right-radius: 10px; border-bottom-right-radius: 10px; border-top-left-radius: 10px; border-bottom-left-radius: 10px; border-image: url("icon.png"); '
      if not self.toggled:
        style = style + 'background-color: rgb(255, 0, 0);}'
      else:
        style = style + 'background: transparent;}'

      self.button.setStyleSheet(style)
      self.label_animation.setPaused(self.toggled)
      self.toggled = not self.toggled

def main():

    app = QApplication(argv)
    ex = Example()
    ex.show()
    exit(app.exec_())


if __name__ == '__main__':
    main()